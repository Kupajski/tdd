 class Money implements Expression {
    protected int amount;
    protected String currency;

    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    static Money dollar(int amount){
        return new Money(amount,"USD");
    }

    static Money franc(int amount){
        return new Money(amount,"CHF");
    }

    Money times(int multiplier){
        return new Money(amount*multiplier,currency);
    }

    Expression plus(Money addend){
        return new Money(amount + addend.amount,currency);
    }

    String currency(){
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        Money money = (Money) o;
        return amount == money.amount && currency.equals(money.currency);
    }
}
